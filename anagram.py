from collections import defaultdict
import itertools
import pickle
import re

__author__ = 'sam'

PICKLE_FILE_PATH = 'anagram.pickle'

class AnagramSolver(object):
    def __init__(self, letters, min_length=2, max_length=-1, pattern=None, dictionary_text_file='words.txt', force_reload=False):
        self.letters = letters
        self.dictionary_text_file = dictionary_text_file
        self.force_reload = force_reload
        self.dictionary = defaultdict(list)
        if max_length == -1:
            max_length = len(letters) + 1
        else:
            max_length += 1
        self.max_length = max_length
        self.min_length = min_length
        self._pattern = None
        if pattern:
            self._pattern = pattern

    @property
    def pattern(self):
        return self._pattern

    @pattern.setter
    def pattern(self, pattern_text):
        self._pattern = re.compile(pattern_text)


    def _load_dictionary(self, input_file_path):
        def load_from_file():
            with open(input_file_path, 'rt') as input_file:
                for w in input_file.readlines():
                    w = w.strip()
                    w2 = ''.join(sorted(w))
                    self.dictionary[w2].append(w)

            with open(PICKLE_FILE_PATH, 'wb') as pickle_file:
                pickle.dump(self.dictionary, pickle_file)

        if self.force_reload:
            load_from_file()
        else:
            try:
                with open(PICKLE_FILE_PATH, 'rb') as pickle_file:
                    self.dictionary = pickle.load(pickle_file)
            except IOError:
                load_from_file()


    def letter_combinations(self):
        combs = list()
        # +1 to include the combinations of all the letters
        for i in range(self.min_length, self.max_length):
            combs.extend([''.join(x) for x in itertools.combinations(self.letters, i)])
        return combs


    def get_suggestions(self):
        if self.dictionary:
            pass
        else:
            self._load_dictionary(self.dictionary_text_file)

        combs = self.letter_combinations()
        combs = set([''.join(sorted(x)) for x in combs])

        suggestions = list()
        for c in combs:
            if c in self.dictionary:
                suggestions.extend(self.dictionary[c])

        if self._pattern:
            suggestions = [x for x in suggestions if self.pattern.match(x)]
        return suggestions


if __name__ == '__main__':
    pass
