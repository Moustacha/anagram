#!/usr/bin/env python3
# encoding: utf-8
'''
anagram cli -- shortdesc

@author:     Sam Shaw

@copyright:  2014 Sam Shaw. All rights reserved.

@license:    WTF

@contact:    moustacha@gmail.com
@deffield    updated: Updated
'''
import sys
import os
from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter

from anagram import AnagramSolver


__author__ = 'sam'
__all__ = []
__version__ = 0.1
__date__ = '2014-01-03'
__updated__ = '2014-01-03'

class CLIError(Exception):
    '''Generic exception to raise and log different fatal errors.'''
    def __init__(self, msg):
        super(CLIError).__init__(type(self))
        self.msg = "E: %s" % msg
    def __str__(self):
        return self.msg
    def __unicode__(self):
        return self.msg

def main(argv=None): # IGNORE:C0111
    '''Command line options.'''

    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    program_name = os.path.basename(sys.argv[0])
    program_version = "v%s" % __version__
    program_build_date = str(__updated__)
    program_version_message = '%%(prog)s %s (%s)' % (program_version, program_build_date)
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    program_license = '''%s

  Created by Sam Shaw on %s.
  Copyright 2014 organization_name. All rights reserved.

  Licensed under the Apache License 2.0
  http://www.apache.org/licenses/LICENSE-2.0

  Distributed on an "AS IS" basis without warranties
  or conditions of any kind, either express or implied.

USAGE
''' % (program_shortdesc, str(__date__))

    try:
        # Setup argument parser
        parser = ArgumentParser(description=program_license, formatter_class=RawDescriptionHelpFormatter)
        parser.add_argument('-V', '--version', action='version', version=program_version_message)
        parser.add_argument('-w', '--words-dict', action='store', help='Path to a text file containing a list of words. Implies -f')
        parser.add_argument('-f', '--force-reload', action='store_true', help='Force reloading the word dictionary', default=False)
        parser.add_argument('--min', action='store', help='Minimum length of anagrams to find %(default)s', default=2)
        parser.add_argument('--max', action='store', help='Maximum length of anagrams to find %(default)s', default=-1)
        parser.add_argument('-p', '--pattern', action='store', help='Regex pattern for matching word suggestions (You might need to escape based on your console)')
        parser.add_argument(dest="letters", help="Letters to find anagrams for", metavar="letters")

        # Process arguments
        args = parser.parse_args()

        letters = args.letters
        words_dict = args.words_dict
        force_reload = args.force_reload
        min_length = int(args.min)
        max_length = int(args.max)
        pattern = args.pattern

        anagram_solver = AnagramSolver(letters=letters, force_reload=force_reload, min_length = min_length, max_length=max_length)
        if words_dict:
            anagram_solver.dictionary_text_file = words_dict
            anagram_solver.force_reload = True

        if pattern:
            anagram_solver.pattern = pattern

        words = anagram_solver.get_suggestions()
        words.sort()
        words.sort(key=len)
        print('\n'.join(words))

        return 0
    except KeyboardInterrupt:
        ### handle keyboard interrupt ###
        return 0
    except Exception as e:
        indent = len(program_name) * " "
        sys.stderr.write(program_name + ": " + repr(e) + "\n")
        sys.stderr.write(indent + "  for help use --help")
        return 2

if __name__ == "__main__":
    sys.exit(main())